<!DOCTYPE HTML>

<html lang="en-US">
<meta charset="UTF-8">
<title>Story Page</title>
<body>

<?php
session_start();
require 'database.php';

$id = $_SESSION['id'] = $mysqli->real_escape_string($_GET['ID']);
// Use a prepared statement
$stmt = $mysqli->prepare("select username, story_title, story_content from stories where id=?");

// Bind the parameter
$stmt->bind_param('s', $id);
$stmt->execute();

// Bind the results
$stmt->bind_result($user, $theStoryTitle, $theStoryContent);
$stmt->fetch();
$stmt->close();

//$_SESSION['id']=$id;
$_SESSION['storytitle']=htmlspecialchars($theStoryTitle);

echo "Story Title: "."<u>".htmlspecialchars($theStoryTitle)."</u>";
echo "<ul>\n";
echo htmlspecialchars($theStoryContent);
echo "<br /><br /><br />";

$stmt = $mysqli->prepare("select username, comments from commentary where comment_id = '$id'");
if(!$stmt){
    //printf("Query Prep Failed: %s\n", $mysqli->error);
    //exit;
    echo "No comments yet.";
}else{
    $stmt->execute();
    $stmt->bind_result($user, $comment);

    echo "<u>Comments by users</u>";
    while($stmt->fetch()){
        printf("\t<li>%s %s</li>",
		htmlspecialchars($user).": ",
		htmlspecialchars($comment)
	);
        if ($user==$_SESSION[htmlspecialchars('username')]){
        ?>
        <form action="edit.php" method="POST">
            <input type='hidden' name='comment' value='<?php echo "$comment";?>'/>
            <input type='hidden' name='title' value='<?php echo "$theStoryTitle";?>'/>
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            <input type='hidden' name='id' value='<?php echo "$id";?>'/>
            <input type="submit" value="Edit">
        </form>

        <form action="delete.php" method="POST">
            <input type='hidden' name=commentmade value='<?php echo "$comment"; ?>'/>
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            <input type="submit" value="Delete comment">
        </form>
        <?php
        }
        echo "<br/><br/>";
    }
}
$stmt->close();
?>

<form action= "comment.php" method="POST">
    <textarea name="comment" placeholder="Type in some comments" rows="2" cols="30"></textarea><br/>
    <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
    <input type="submit" value="Post your comment">
</form>
</body>
</html>