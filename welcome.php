<!DOCTYPE html>

<?php
session_start();
?>

<head>
<html lang="en-US">
<meta charset="UTF-8">
<title>Welcome page</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
    <body>
        <h1>
            Welcome to a great blog site of world events!
        </h1>
        <h3>Add a story:</h3>
        <a href='registration.php'>Click here to login or register to add stories and/or comments</a><br />
        <form action="addstory.php" method="POST">
            <p>
		        <input type="text" placeholder="Type in the Title of your story" name="storytitle" id="storytitle" size="43"/><br />
	        </p>
	        <p>
                <textarea name="storytext" placeholder="Type or paste the text of your story" rows="5" cols="40"></textarea><br/>
                and/or, <input type="text" placeholder="Type or paste the URI of your story" name="storyuri" id="storyuri" size="36"/>
                <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                <input type="submit" value="Add story" />
	        </p>
        </form><br/><br/>
        <h3>
            Welcome to what's happening in the world from our members!
        </h3>
<?php
//session_start();
require 'database.php';
$stmt = $mysqli->prepare("select id, username, story_title, story_content, uri, likes, dislikes from stories order by username");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
// $stmt->bind_result($id, $user, $title, $theStoryContent, $uri);
$result = $stmt->get_result();

echo "<ul>\n";
// Referenced https://www.youtube.com/watch?v=39Kd1UCQu8M for using hrefs icw mysql.
while($row = $result->fetch_assoc()){
    printf("\t<li>username: %s</li>", htmlspecialchars($row['username']));
    $id = htmlspecialchars($row['id']);
    $storyTitle = htmlspecialchars($row['story_title']);
    echo "Link to story: ";
    echo "<a href='viewstory(alt).php?ID=$id'>$storyTitle</a><br />";
    $uri = htmlspecialchars($row['uri']);
    //$keywords = preg_split("/[\/]/", "$uri");
    //print_r($keywords);
    //echo "<a href=$uri>$uri</a><br /><br />";
    //echo $keywords[0]."<br />";
    // echo "<a href='$keywords[0]' target=_blank>$uri</a><br /><br />";
    echo "<a href='http://$uri' target=_blank>$uri</a><br /><br />";
    echo "Likes: ".htmlspecialchars($row['likes'])."<br/>";
    echo "Dislikes: ".htmlspecialchars($row['dislikes']);
?>
    
    <form action="like.php" method="POST">
    <input type='submit' value='Like'>
    <input type='hidden' name='story_title' value='<?php echo htmlspecialchars($row['story_title']);?>'/>
    <input type='hidden' name='likes' value='<?php echo htmlspecialchars($row['likes']);?>'/>
    </form>
    <form action="dislike.php" method="POST">
    <input type='hidden' name='story_title' value='<?php echo htmlspecialchars($row['story_title']);?>'/>
    <input type='hidden' name='dislikes' value='<?php echo htmlspecialchars($row['dislikes']);?>'/>
    <input type='submit' value='Disike'>
    </form>

<?php
    if ( htmlspecialchars($row['username'])==htmlspecialchars($_SESSION['username'])){
?>
        <form action="delete.php" method="POST">
            <input type='hidden' name='storycontent' value='<?php echo htmlspecialchars($row['story_content']);?>'/>
            <input type='hidden' name='storyid' value='<?php echo "$id";?>'/>
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            <input type="submit" value="delete">
        </form>
        
        <form action="edit.php" method="POST">
            <input type='hidden' name='comment' value='<?php echo "$theStoryContent";?>'/>
            <input type='hidden' name='title' value='<?php echo "$theStoryTitle";?>'/>
            <input type="submit" value="Edit">
        </form>
<?php
        }
}
echo "</ul>\n";
$stmt->close();
?><br /><br /><br /><br />

        <form action = "logout.php">
        <input type = 'submit' value = 'Logout'>
        </form>
    </body>
</html>