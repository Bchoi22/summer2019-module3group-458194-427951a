<!DOCTYPE HTML>
<html lang="en-US">
<meta charset="UTF-8">
    <title>Delete Page</title>
    <body>

<?php
session_start();
require 'database.php';

if (htmlspecialchars($_SESSION['username']) != NULL && htmlspecialchars($_SERVER['REQUEST_METHOD']) == 'POST'){
    $sess_token = $mysqli->real_escape_string($_SESSION['token']);
    $post_token = $mysqli->real_escape_string($_POST['token']);
    if(!hash_equals($sess_token, $post_token)){
	    die("Request forgery detected");
    }
    //$mysqli->query(/* perform transfer */);
    echo "working....";
    //$id = htmlspecialchars($_SESSION['id']);
    if (htmlspecialchars($_POST['commentmade'])){
        $commentmade=$mysqli->real_escape_string($_POST['commentmade']);
        echo $commentmade;
        echo "a;dlsfkjasl;fj";
        $stmt = $mysqli->prepare("delete from commentary where comments=?");
        $stmt->bind_param('s', $commentmade);
    }
    if (htmlspecialchars($_POST['storyid'])){
        $storyid=$mysqli->real_escape_string($_POST['storyid']);
        $stmt = $mysqli->prepare("delete from stories where id=?");
        $stmt->bind_param('s', $storyid);
    }

    echo $storyid."<br>";
    //echo $commentmade;
    
    if(!$stmt){
        die("Deletion failed");
	    //printf("Query Prep Failed: %s\n", $mysqli->error);
	    //exit;
    }
    $stmt->execute();
    $stmt->close();

    $url=htmlspecialchars($_SERVER['HTTP_REFERER']);
    echo "Delete success.  Redirecting back...";
    header("Refresh: 1; URL=$url");
}
?>
    </body>
</html>